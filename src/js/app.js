'use strict';

$(document).ready(function()
{
	console.log('%c Made with love by TBNT - Lausanne, Switzerland', 'background: #202542; color: #EF604C;');
	console.log('%c Visit website http://tbnt.digital/', 'background: #202542; color: #EF604C;');


	setTimeout(function() {
		$('body').addClass('loading');
	}, 100);

	setTimeout(function() {
		$('body').addClass('loaded');
		$('html,body').scrollTop(0);
	}, 1100);

	setTimeout(function() {
		$('html').removeClass('noscroll');
	}, 1600);

	setTimeout(function() {
		$('body').addClass('notransition');
		$('.loader').addClass('notransition');
	}, 4000);


	//WOW
	var wow = new WOW()
	wow.init();


	//SKROLLR
	var s = null;

	$(function (){
		// initialize skrollr if the window width is large enough
		if ($(window).width() > 769) {
			s = skrollr.init({
				forceHeight: false
			});
		}

		// disable skrollr if the window is resized below 768px wide
		$(window).on('resize', function () {
			if ($(window).width() <= 769) {
				if(s != null){
					s.destroy();
					s = null;
				}
			}
			else if ($(window).width() > 769) {
				if(s == null){
					s = skrollr.init({
						forceHeight: false
					});
				}
				s.refresh();
			}
		});

	});


	$('.read-more').on('click', function(){

		$(this).toggleClass('more');

		var txt = $(this).hasClass('more') ? 'Read less' : 'Read more';
		var p = $(this).parent().find('p');
		var h = $('.overflow-text').hasClass('more') ? 90 : p.height();

		$(this).text(txt);

		p.toggleClass('more');
		$('.overflow-text').toggleClass('more');

		$('.overflow-text').css('height', h+'px');
	});


	$('.select-plan-button.disable').on('click', function(){
		return false;
	});

	$(window).on('resize', function () {
		if ($(window).width() > 769) {
			$('.overflow-text').css('height', '');
		}
	});



	//input
	$('input[type=email]').on('focus', function(){
		$(this).parent().addClass('focus');
	});

	$('input[type=email]').on('focusout', function(){
		$(this).parent().removeClass('focus');
	});




});
