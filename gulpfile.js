'use strict';

var gulp 			= require('gulp');
var $ 				= require('gulp-load-plugins')({scope: ['devDependencies']});
var fs 				= require('fs');
var browserSync 	= require('browser-sync').create();
var yaml 			= require('yamljs');
var pngquant 		= require('imagemin-pngquant');
var argv 			= require('optimist').argv;
var cleanCSS 		= require('gulp-clean-css');
var map 			= require('map-stream');
var js 				= require('js-yaml');
var gutil 			= require("gulp-util");
var os 				= require('os');
var path 			= require('path');
var jsonSass 		= require('gulp-json-scss');
var sass 			= require('gulp-ruby-sass');


var destSite 	= './build/';


//------------------------------------------------------------------------------
//BrowserSync-------------------------------------------------------------------



gulp.task('watch', function(gulpCallback) {
	var browser = browser === undefined ? 'google chrome' : browser;
	browserSync.init({
		server: './build/',
		open: true,
		browser: browser
	}, function callback() {
		gulp.watch('src/*.html', ['index']);

		gulp.watch('src/sass/**/*.sass', ['sass']);
		gulp.watch('src/sass/*.sass', ['sass']);

		gulp.watch('src/assets/fonts/*', ['assets']);
		gulp.watch('src/assets/icons/*', ['assets']);
		gulp.watch('src/assets/images/*',['assets']);
		gulp.watch('src/assets/video/*',['assets']);


		gulp.watch('src/js/**/*',['script', 'eslint']);

		gulp.watch('src/*.html', browserSync.reload);
		gulp.watch('src/js/**/*', browserSync.reload);

		gulpCallback();
	});
});



//------------------------------------------------------------------------------
//compile jade into html page---------------------------------------------------



gulp.task('index', function(){
	gulp.src(['src/index.html', 'src/pricing.html', 'src/terms.html'])
	.pipe($.htmlmin({collapseWhitespace: true}))
	.pipe(gulp.dest(destSite))
	.pipe(browserSync.stream());
});


//------------------------------------------------------------------------------
//sass--------------------------------------------------------------------------



gulp.task('sass', function() {
	return gulp.src(['src/sass/app.sass'])
	.pipe($.cssGlobbing({
		extensions: ['.sass']
	}))
	.pipe($.sass({
		outputStyle: 'compressed'
	})
	.on('error', $.sass.logError))
	.on("error", $.notify.onError({
		message: 'You know sometimes, shit happens: <%= error.message %>'
	}))
	.pipe($.autoprefixer({
			cascade: false
	 }))
	.pipe(cleanCSS({
			compatibility: 'ie8'
	}))
	.pipe(gulp.dest(destSite+'css/'))
	.pipe(browserSync.stream({match: '**/*.css'}));
});


//------------------------------------------------------------------------------
//scripts-----------------------------------------------------------------------



gulp.task('script', function() {
	return gulp.src([
		'node_modules/jquery/dist/jquery.js',
		'node_modules/lodash/lodash.min.js',
		'node_modules/gsap/src/uncompressed/TweenMax.js',
		'src/js/vendor/wow/wow.min.js',
		'src/js/vendor/skrollr/skrollr.min.js',
		'src/js/modules/**/*',
		'src/js/app.js',
	])
	.pipe($.concat('app.js'))
	//.pipe($.uglify())
	.pipe(gulp.dest(destSite+'js/'))
	.pipe(browserSync.stream());
});



//------------------------------------------------------------------------------
//eslint------------------------------------------------------------------------

gulp.task('eslint', function() {
	return gulp.src('src/js/*.js')
	.pipe($.eslint())
	.pipe($.eslint.format())
	.pipe($.eslint.failOnError());
});


//------------------------------------------------------------------------------
//assets------------------------------------------------------------------------

function copyfiles(src, dest){
	return gulp.src(src)
	.pipe(gulp.dest(dest));
}

gulp.task('assets', function() {
	copyfiles('src/assets/fonts/*/*','build/assets/fonts');
	copyfiles('src/assets/icons/*','build/assets/icons');
	copyfiles('src/assets/videos/*','build/assets/videos');
	copyfiles('src/assets/images/**/*','build/assets/images');
	copyfiles('src/tutorial/**/*','build/tutorial');
});


//------------------------------------------------------------------------------
//Build Task--------------------------------------------------------------------

gulp.task('default', [
	'index',
	'sass',
	'script',
	'eslint',
	'assets',
	'watch'
]);
